# Integração Continua em projetos iOS usando Gitlab CI



### Primeiros passos:

Antes de tudo, você precisa saber que o `Gitlab CI` usa seu *Mac* para construir a sua build, executar seus testes e outras coisas a mais.  
Então, antes de você começar, configure o seu `macOS` para se comunicar certinho com o `Gitlab` e você não ficar quebrando cabeça lá na frente. ;
)


### Recomendo usar o SHELL como `executor` do seu build no `gitlab-ci` 


O *Shell Fish* é mais simples e fácil de configurar. 

```
O Docker me deu bastante trabalho e não funcionou. :(
```

### Abra seu terminal e digite:


```
brew install fish
```

Após instalado, adicione `Fish` para ```/etc/shells```:

```
echo "/usr/local/bin/fish" | sudo tee -a /etc/shells
```

Caso tenha dúvidas aqui está um [link maroto](https://hackercodex.com/guide/install-fish-shell-mac-ubuntu/) de como instalar o `Shell Fish` no *Mac* e no *Linux*(Ubuntu)


Pronto, esse é o principal `problema` e `detalhe` que os tutoriais na internet não falam e você fica quebrabdo cabeça a toa.


Depois disso, os links do próprio `Gitlab CI` já são de boa, é so fazer conforme que da tudo certo.


### Segue as configuração nessa ordem:


1.[Configurando projeto iOS e criando seus `Runners`](https://about.gitlab.com/2016/03/10/setting-up-gitlab-ci-for-ios-projects/)


2.[Instalando o Gitlab CI no seu Mac](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md)



Espero que eu possa ter ajudado! :)

Qualquer coisa se tiver problemas, cria uma [Nova Issue](https://gitlab.com/douglasalexandre/demoios-ci/issues/new) que eu tento ajudar! ;)

*Bons estudos!*